package com.idfy.core.socket;

import org.json.JSONObject;

public interface SocketCallback{
    void onConnected(String roomId,String participantId);
    void setRoomCall(JSONObject roomCall);

    void stopConnection();

    void onReconnecting();

    void setScreenShotUpload(String artifact, String link);

    void onDisconnect(String code);

}
