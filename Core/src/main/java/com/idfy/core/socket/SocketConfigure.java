package com.idfy.core.socket;

import com.fasterxml.jackson.databind.node.ObjectNode;

import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.ISocketOpenCallback;
import org.phoenixframework.channels.Socket;

import java.io.IOException;

public class SocketConfigure {

    private String webSocketUrl = "";
    private Socket socket;
    private Channel channel;
    private SocketConfigCallBack callBack;
    private int errorCtr = 0;

    public SocketConfigure(String webSocketUrl, SocketConfigCallBack callBack) {
        this.webSocketUrl = webSocketUrl;
        this.callBack = callBack;
    }

    public void setUpSocket() {
        try {
            socket = new Socket(webSocketUrl, 5000);
            socket.reconectOnFailure(true);
            socket.connect();
            socket.onOpen(new ISocketOpenCallback() {
                @Override
                public void onOpen() {
                    System.out.println("Capture socket Open");
                }});

            socket.onClose(() -> {
                System.out.println("Capture socket close");
                callBack.onSocketError("Socket Connection Closed");

            });

            socket.onError(reason -> {
                System.out.println("Capture socket Error "+reason);
                /*if (reason.contains("403")){
                    if (errorCtr >=2){
                        System.out.println("Try to reconnect capture socket 403 error ctr: "+errorCtr);
                        try{
                            socket.disconnect();
                            socket.connect();
                            channel.join();
                        }catch (Exception e){

                        }
                        errorCtr = 0;
                    }else {
                        errorCtr++;
                    }
                }else {
                    callBack.onSocketError(reason);
                }*/
                callBack.onSocketError(reason);
            });


            callBack.initSocket(socket);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setupChannel(Socket socket, String topic,ObjectNode payload){
        channel = socket.chan(topic, payload);
        if (channel != null) {
            callBack.initChannel(channel);
        }
    }

    private void disconnectSocket(){
        try {
            System.out.println("Disconnecting socket ");
            channel.leave();
            socket.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
