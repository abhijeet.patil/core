package com.idfy.core.socket;

import android.net.Uri;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.idfy.core.api.ApiCallBack;

import org.json.JSONException;
import org.json.JSONObject;
import org.phoenixframework.channels.*;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class PhoenixRoomIdSocket {
    Socket socket;
    Channel channel;

    private String webSocketUrl = "wss://ms.idfystaging.com/controller/socket/websocket";
    private String participantId = "";
    private String roomId = "";

    String streamSessionId;
    SocketCallback callback;
    Executor executor = Executors.newSingleThreadExecutor();
    private boolean isChannelStarted = false;


    public PhoenixRoomIdSocket(String roomId, String participantId, SocketCallback callback) {
        this.roomId = roomId;
        this.participantId = participantId;
        this.callback = callback;
    }

    public void connectSocket() {
        try {

            Uri.Builder url = Uri.parse(webSocketUrl).buildUpon();
            url.appendQueryParameter("participant_id", this.participantId);

            socket = new Socket(url.build().toString());
            Log.d("==socket-url", url.build().toString()+" "+roomId);

            socket.onClose(() -> {
                Log.d("Socket", "Socket connection closed");
                try {
                    socket.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            socket.onError(reason -> {
                //handleTerminalError(reason);
                Log.d("Socket", "Socket connection error: " + reason);
            });
            socket.onOpen(new ISocketOpenCallback() {
                @Override
                public void onOpen() {
                    System.out.println("Socket is open");
                }
            });
            socket.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        channel = socket.chan("room:" + roomId, new ObjectNode(JsonNodeFactory.instance));

        try {
            channel.join()
                    .receive("ok", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {
                            System.out.println("JOINED with Room socket" + envelope.toString());
                        }
                    })
                    .receive("error", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {
                            System.out.println("Error Room Socket " + envelope.getReason() + " " + envelope.getPayload());
                        }
                    });

            channel.on("start", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    Log.d("===WEBSOCKET", "START"+envelope.getPayload());
                    if (!isChannelStarted) {
                        String roomID = envelope.getPayload().get("ms_room_reference_id").asText();
                        int msRoomId = envelope.getPayload().get("ms_room_id").asInt();
                        String janusLink = envelope.getPayload().get("instance").get("link").asText();
                        try {
                            JSONObject startObject = new JSONObject(envelope.getPayload().toString());
                            callback.setRoomCall(startObject);
                            isChannelStarted = true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            channel.on("reconnecting", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    Log.d("===RECONNECT", "RECONNECTING START" + envelope.getPayload());
                    System.out.println("reconnecting " + envelope.getReason() + " " + envelope.getPayload());
                    String participant_id = envelope.getPayload().get("participant_id").asText();
//                    if (participant_id.equals(participantId)) {
//                        Log.d("===RECONNECT", "RECONNECTING RETURN");
//                        return;
//                    }
                    isChannelStarted = false;
                    participantId = participant_id;
                    callback.onReconnecting();
                    //disconnect();
                    //connectSocket();

                }
            });

            channel.on("new:msg", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("NEW MESSAGE: " + envelope.toString());
                }
            });
            channel.on("connected", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("connected: " + envelope.toString());
                }
            });

            channel.on(ChannelEvent.CLOSE.getPhxEvent(), new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("CLOSED: " + envelope.toString());
                }
            });

            channel.on(ChannelEvent.ERROR.getPhxEvent(), new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("MS socket ERROR: " + envelope.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendEvent(String event, JSONObject jsonObject, ApiCallBack payApiCallBack) {
        //  Log.d("===RECONNECT", event + " " + jsonObject.toString());
        Log.d("===ROOMID", "RECONNECTING ROOM ID " +channel.getTopic());

        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonNode = mapper.readTree(jsonObject.toString());

                    channel.push(event, jsonNode)
                            .receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    Log.i("TAG", "6,10 : " + envelope);
                                    JSONObject jsonObject = null;
                                    try {
                                        String result = envelope.getPayload().get("response").toString();
                                        jsonObject = new JSONObject(result);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    payApiCallBack.onSessionSuccess(jsonObject);
                                }
                            })
                            .timeout(new ITimeoutCallback() {
                                @Override
                                public void onTimeout() {
                                    Log.d("===RECONNECT", "TIMEOUT");
                                }
                            });

                } catch (Exception e) {
                    System.out.println("==>" + e);
                }
            }
        });
    }


    public void disconnect() {
        // channel = null;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {

                    if (socket != null) {
                        socket.disconnect();
                        // socket = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }
    public void connect() {
        // channel = null;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (socket != null) {
                        socket.connect();
                        // socket = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


}
