package com.idfy.core.model;

public class ThemeConfig {
    private String headerBackgroundColor;
    private String headerTextColor;
    private boolean isDisplayFooter = false;
    private String secondaryMainColor;
    private String secondaryContrastColor;
    private String primaryMainColor;
    private String primaryContrastColor;
    private String logo;


    public String getHeaderBackgroundColor() {
        return headerBackgroundColor;
    }

    public void setHeaderBackgroundColor(String headerBackgroundColor) {
        this.headerBackgroundColor = headerBackgroundColor;
    }

    public String getHeaderTextColor() {
        return headerTextColor;
    }

    public void setHeaderTextColor(String headerTextColor) {
        this.headerTextColor = headerTextColor;
    }

    public boolean isDisplayFooter() {
        return isDisplayFooter;
    }

    public void setDisplayFooter(boolean displayFooter) {
        isDisplayFooter = displayFooter;
    }

    public String getSecondaryMainColor() {
        return secondaryMainColor;
    }

    public void setSecondaryMainColor(String secondaryMainColor) {
        this.secondaryMainColor = secondaryMainColor;
    }

    public String getSecondaryContrastColor() {
        return secondaryContrastColor;
    }

    public void setSecondaryContrastColor(String secondaryContrastColor) {
        this.secondaryContrastColor = secondaryContrastColor;
    }

    public String getPrimaryMainColor() {
        return primaryMainColor;
    }

    public void setPrimaryMainColor(String primaryMainColor) {
        this.primaryMainColor = primaryMainColor;
    }

    public String getPrimaryContrastColor() {
        return primaryContrastColor;
    }

    public void setPrimaryContrastColor(String primaryContrastColor) {
        this.primaryContrastColor = primaryContrastColor;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
