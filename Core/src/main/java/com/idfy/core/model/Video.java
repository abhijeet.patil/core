package com.idfy.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Video {
    @SerializedName("codec")
    @Expose
    private String codec;
    @SerializedName("bytes")
    @Expose
    private int bytes;
    @SerializedName("bandwidth")
    @Expose
    private Double bandwidth;
    @SerializedName("averageBandwidth")
    @Expose
    private Double averageBandwidth;
    @SerializedName("latency")
    @Expose
    private int latency;
    @SerializedName("packetsLost")
    @Expose
    private int packetsLost;
    @SerializedName("codecImplementationName")
    @Expose
    private String codecImplementationName;
    @SerializedName("jitter")
    @Expose
    private Integer jitter = 0;
    @SerializedName("jitterBufferDelay")
    @Expose
    private Integer jitterBufferDelay = 0;

    public String getCodec() {
        return codec;
    }

    public void setCodec(String codec) {
        this.codec = codec;
    }

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    public Double getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Double bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Double getAverageBandwidth() {
        return averageBandwidth;
    }

    public void setAverageBandwidth(Double averageBandwidth) {
        this.averageBandwidth = averageBandwidth;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public int getPacketsLost() {
        return packetsLost;
    }

    public void setPacketsLost(int packetsLost) {
        this.packetsLost = packetsLost;
    }

    public String getCodecImplementationName() {
        return codecImplementationName;
    }

    public void setCodecImplementationName(String codecImplementationName) {
        this.codecImplementationName = codecImplementationName;
    }

    public Integer getJitter() {
        return jitter;
    }

    public void setJitter(Integer jitter) {
        this.jitter = jitter;
    }

    public Integer getJitterBufferDelay() {
        return jitterBufferDelay;
    }

    public void setJitterBufferDelay(Integer jitterBufferDelay) {
        this.jitterBufferDelay = jitterBufferDelay;
    }
}
