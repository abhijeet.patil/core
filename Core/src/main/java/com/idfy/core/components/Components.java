package com.idfy.core.components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.Gravity;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.idfy.core.R;
import com.idfy.core.components.ModelComponents.ModelButton;
import com.idfy.core.components.ModelComponents.ModelCardView;
import com.idfy.core.components.ModelComponents.ModelCheckBox;
import com.idfy.core.components.ModelComponents.ModelImageView;
import com.idfy.core.components.ModelComponents.ModelLinearLayout;
import com.idfy.core.components.ModelComponents.ModelTextInputLayout;
import com.idfy.core.components.ModelComponents.ModelTextView;
import com.idfy.core.components.ModelComponents.ModelToolBar;

public class Components {
    private Context context;

    public Components(Context context) {
        this.context = context;
    }

    public Toolbar getToolBar(ModelToolBar toolBar){

        Toolbar toolbar = new Toolbar(context);
        toolbar.setBackgroundColor(Color.parseColor(toolBar.getBackgroundColor()));
        toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        return toolbar;
    }

    public ImageView getImageView(ModelImageView imageViewModel){

        ImageView imageView = new ImageView(context);
        if (imageViewModel.getResource() != 0) {
            imageView.setBackgroundResource(imageViewModel.getResource());
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(imageViewModel.getWidth(), imageViewModel.getHeight());
        params.setMargins(imageViewModel.getMarginLeft(), imageViewModel.getMarginTop(),
                imageViewModel.getMarginRight(), imageViewModel.getMarginBottom());
        imageView.setLayoutParams(params);

        return imageView;
    }

    public TextView getTextView(ModelTextView textViewModel){
        TextView textView = new TextView(context);
        textView.setText(textViewModel.getText());
        textView.setTextSize(textViewModel.getSize());
        textView.setTypeface(textView.getTypeface(), textViewModel.setFontStyle(textViewModel.getTypeFace()));
        if (textViewModel.getGravity() == 0) {
            textView.setGravity(textViewModel.getGravity());
        }
        textView.setTextColor(Color.parseColor(textViewModel.getTextColor()));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(textViewModel.getWidth(), textViewModel.getHeight());
        params.setMargins(textViewModel.getMarginLeft(), textViewModel.getMarginTop(),
                textViewModel.getMarginRight(), textViewModel.getMarginBottom());
        textView.setLayoutParams(params);
        return textView;
    }



    public CardView getCardView(ModelCardView cardViewModel){

        CardView cardView = new CardView(context);
        cardView.setCardBackgroundColor(Color.parseColor(cardViewModel.getBackgroundColor()));
        cardView.setElevation(cardViewModel.getElevation());
        cardView.setRadius(cardViewModel.getRadius());

        //CardView.LayoutParams params = new FrameLayout.LayoutParams(CardView.LayoutParams.MATCH_PARENT, CardView.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(cardViewModel.getWidth(), cardViewModel.getHeight());
        params.setMargins(cardViewModel.getMarginLeft()
                , cardViewModel.getMarginTop(), cardViewModel.getMarginRight(), cardViewModel.getMarginBottom());
        cardView.setPadding(cardViewModel.getPaddingLeft(),cardViewModel.getPaddingTop(),
                cardViewModel.getPaddingRight(),cardViewModel.getPaddingBottom());

        cardView.setLayoutParams(params);

        return cardView;
    }
    public LinearLayout getLinearLayout(ModelLinearLayout modallinear){
        LinearLayout layout = new LinearLayout(context);
        layout.setBackgroundColor(modallinear.getBackgroundColor());
        layout.setElevation(modallinear.getElevation());
        layout.setOrientation(modallinear.getOrientation());
        if (modallinear.getGravity() != 0) {
            layout.setGravity(modallinear.getGravity());
        }
        if (modallinear.getId() != -1) {
            layout.setId(modallinear.getId());
        }
        layout.setPadding(modallinear.getPaddingLeft(),modallinear.getPaddingTop(),modallinear.getPaddingRight(),modallinear.getPaddingBottom());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(modallinear.getWidth(), modallinear.getHeight(),modallinear.getWeight());
        params.setMargins(modallinear.getMarginLeft(), modallinear.getMarginTop(), modallinear.getMarginRight(), modallinear.getMarginBottom());
        layout.setLayoutParams(params);
        return layout;
    }

    public Button getButton(ModelButton modButton) {
        Button btn = new Button(context);
        btn.setText(modButton.getText());
       // btn.setBackgroundColor(Color.parseColor(modButton.getBackgroundColor()));
        btn.getBackground().setColorFilter(Color.parseColor(modButton.getBackgroundColor()), PorterDuff.Mode.MULTIPLY);
        btn.setTextSize(modButton.getSize());
        btn.setTypeface(btn.getTypeface(), modButton.getTypeFace());  //Typeface.BOLD_ITALIC
        btn.setGravity(Gravity.CENTER);
        btn.setTextColor(Color.parseColor(modButton.getTextColor()));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(modButton.getWidth(), modButton.getHeight());
        params.setMargins(modButton.getMarginLeft(), modButton.getMarginTop(),modButton.getMarginRight(), modButton.getMarginBottom());
        btn.setLayoutParams(params);
        return btn;
    }

    public RadioGroup getRadioGroup(int orientation) {
        RadioGroup rg = new RadioGroup(context);
        rg.setOrientation(RadioGroup.VERTICAL);    // RadioGroup.VERTICAL
        return rg;
    }
    public RadioButton getRadioButton(String text, int id) {
        RadioButton rb = new RadioButton(context);
        rb.setText(text);
        rb.setId(id);
        return rb;
    }
    public CheckBox getCheckBox(ModelCheckBox modelCheckBox) {
        CheckBox chkbx = new CheckBox(context);
        chkbx.setText(modelCheckBox.getText());
        chkbx.setTextColor(Color.parseColor(modelCheckBox.getTextColor()));
        chkbx.setPadding(modelCheckBox.getPaddingLeft(),modelCheckBox.getPaddingTop(),modelCheckBox.getPaddingRight(),modelCheckBox.getPaddingBottom());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(modelCheckBox.getWidth(), modelCheckBox.getHeight());
        params.setMargins(modelCheckBox.getMarginLeft(), modelCheckBox.getMarginTop(), modelCheckBox.getMarginRight(), modelCheckBox.getMarginBottom());
        chkbx.setLayoutParams(params);
        return chkbx;
    }

    public ViewStub getViewStub(){
        ViewStub viewStub = new ViewStub(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        viewStub.setLayoutParams(params);
        return viewStub;
    }

    public TextInputLayout getTextInputLayout(ModelTextInputLayout modelTextInputLayout){
        TextInputLayout textInputLayout = new TextInputLayout(context,null, R.style.Widget_Design_TextInputLayout);
        TextInputEditText editText = new TextInputEditText(textInputLayout.getContext());
        editText.setHint(modelTextInputLayout.getHint());
       // editText.setHintTextColor(ContextCompat.getColor(context,R.color.black));
       // editText.setTextColor(ContextCompat.getColor(context,R.color.black));
        LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        //textInputLayout.setBoxBackgroundColor(ContextCompat.getColor(context, ));
        textInputLayout.setBoxBackgroundMode(TextInputLayout.BOX_BACKGROUND_OUTLINE);
        LinearLayout.LayoutParams textInputLayoutParams = new LinearLayout.LayoutParams(
                modelTextInputLayout.getWidth(),
                modelTextInputLayout.getHeight());
        textInputLayoutParams.setMargins(modelTextInputLayout.getMarginLeft(),modelTextInputLayout.getMarginTop()
                ,modelTextInputLayout.getMarginRight(),modelTextInputLayout.getMarginBottom());

        textInputLayout.setLayoutParams(textInputLayoutParams);
        textInputLayout.addView(editText,editTextParams);
       // textInputLayout.setHint(modelTextInputLayout.getHint());

        return textInputLayout;
    }
}
