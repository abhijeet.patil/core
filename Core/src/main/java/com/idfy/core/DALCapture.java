package com.idfy.core;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.idfy.core.api.ApiCallBack;
import com.idfy.core.api.ApiInterface;
import com.idfy.core.api.RetrofitInstance;
import com.idfy.core.model.PageSequence;
import com.idfy.core.model.ThemeConfig;
import com.idfy.core.socket.SocketConfigCallBack;
import com.idfy.core.socket.SocketConfigure;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.Envelope;
import org.phoenixframework.channels.IMessageCallback;
import org.phoenixframework.channels.ITimeoutCallback;
import org.phoenixframework.channels.Socket;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DALCapture {

    private SocketConfigure socketConfigure;
    private String token;
    private String captureId;
    private String sessionId;
    private String requestId;
    private Socket captureSocket;
    private Channel captureChannel;
    private ApiCallBack apiCallBack;
    private static DALCapture dalCapture = null;
    private List<PageSequence> listPageSeq = new ArrayList<>();
    private ThemeConfig themeConfig = null;
    private int pageIndex = 0;
    private String status;
    private String latitude;
    private String longitude;
    private String accuracy;
    private String altitude;
    private String speed;
    Executor executor = Executors.newSingleThreadExecutor();


    public DALCapture() {
        //createSession();
    }

    public static DALCapture getInstance() {
        if (dalCapture == null)
            dalCapture = new DALCapture();

        return dalCapture;
    }


    public void getPageData(ApiCallBack apiCallBack) {
        String webSocketUrl = "wss://capture.kyc.idfystaging.com/backend/socket/capture/websocket?" +
                "t=" + token +
                "&capture_id=" + captureId +
                "&session_token=" + sessionId;
        socketConfigure = new SocketConfigure(webSocketUrl, new SocketConfigCallBack() {
            @Override
            public void initSocket(Socket socket) {
                captureSocket = socket;
                socketConfigure.setupChannel(captureSocket, "session:" + captureId, new ObjectNode(JsonNodeFactory.instance));
            }

            @Override
            public void initChannel(Channel channel) {
                captureChannel = channel;
                if (captureChannel != null) {
                    joinChannel(apiCallBack);
                }
            }

            @Override
            public void onSocketError(String message) {
                apiCallBack.onSessionFailure(message);
            }
        });
        socketConfigure.setUpSocket();

    }

    public void disconnectDalSocket() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (captureSocket != null) {
                    try {
                        captureChannel.leave();
                        captureSocket.disconnect();
                        captureChannel = null;
                        captureSocket = null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void parsePageSequence(JSONObject object){
        try {
            listPageSeq.clear();
            JSONArray pageSequence = object.getJSONArray("page_sequence");
            for (int i = 0; i < pageSequence.length(); i++) {
                JSONObject jsonObject = pageSequence.getJSONObject(i);
                String page = jsonObject.optString("page");
                JSONArray validations = jsonObject.getJSONArray("validations");
                listPageSeq.add(new PageSequence(page, validations));

            }
        }catch (Exception e){
        }
    }



    private void joinChannel(ApiCallBack apiCallBack) {
        try {
            captureChannel.join()
                    .receive("ok", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {
                            if (listPageSeq.size() == 0) {
                                JSONObject jsonObject = null;
                                try {
                                    String result = envelope.getPayload().get("response").toString();
                                    jsonObject = new JSONObject(result);
                                    parsePageSequence(jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                apiCallBack.onSessionSuccess(jsonObject);
                            }
                        }
                    })
                    .receive("error", new IMessageCallback() {
                        @Override
                        public void onMessage(Envelope envelope) {
                            apiCallBack.onSessionFailure(envelope.getReason());
                        }
                    });
            captureChannel.on("reconnecting", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("reconnecting " + envelope.getReason() + " " + envelope.getPayload());

                }
            });
            captureChannel.on("connected", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    System.out.println("connected: " + envelope.toString());
                }
            });
            captureChannel.on("_disconnect", new IMessageCallback() {
                @Override
                public void onMessage(Envelope envelope) {
                    String code = envelope.getPayload().get("code").asText();
                    if (code.equals("SESSION_OVERRIDE")) {
                        apiCallBack.intermediateCallBack();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void fetchPageConfig(String event, JSONObject jsonObject, ApiCallBack payApiCallBack) {

        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode jsonNode = mapper.readTree(jsonObject.toString());

                    captureChannel.push(event, jsonNode)
                            .receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    Log.i("TAG", "6,10 : " + envelope);
                                    JSONObject jsonObject = null;
                                    try {
                                        String result = envelope.getPayload().get("response").toString();
                                        jsonObject = new JSONObject(result);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    payApiCallBack.onSessionSuccess(jsonObject);
                                }
                            })
                            .timeout(new ITimeoutCallback() {
                                @Override
                                public void onTimeout() {
                                    Log.w("TAG", "MESSAGE timed out");
                                }
                            });

                } catch (Exception e) {
                    System.out.println("==>" + e);
                }
            }
        });
    }

    public void getCaptureStatusDetails(Context context, String token, boolean override, ApiCallBack apiCallBack) {
        setToken(token);
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class).getCaptureStatus(override, token).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.e("==TAG", "response 33: " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    themeConfig = new ThemeConfig();
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                         sessionId = jsonObject.getJSONObject("body").getString("session_id");
                         captureId = jsonObject.getJSONObject("body").getString("capture_id");
                         status = jsonObject.getJSONObject("body").getString("status");
                        JSONObject headerObject = jsonObject.getJSONObject("header");
                        String logo = headerObject.optString("logo");
                        themeConfig.setLogo(logo);
                        JSONObject themeObject = headerObject.getJSONObject("theme_config");
                        parseThemeConfig(themeObject);
                        apiCallBack.onSessionSuccess(headerObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        apiCallBack.onSessionFailure(jObjError.getString("message"));
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e("==TAG", "onFailure: " + t.getMessage());
                apiCallBack.onSessionFailure(t.getMessage());


            }
        });
    }
    private void parseThemeConfig(JSONObject themeObject) {
        if (themeObject != null) {
           String headerBackgroundColor = themeObject.optJSONObject("custom_header")
                    .optString("backgroundColor");
           themeConfig.setHeaderBackgroundColor(headerBackgroundColor);
            String headerTextColor = themeObject.optJSONObject("custom_header")
                    .optString("color");
            themeConfig.setHeaderTextColor(headerTextColor);
            JSONObject footerObject = themeObject.optJSONObject("custom_footer");
            boolean isDisplayFooter = false;
            if (footerObject != null)
                isDisplayFooter = footerObject.optBoolean("display");
            themeConfig.setDisplayFooter(isDisplayFooter);
            JSONObject paletteObject = themeObject.optJSONObject("palette");
            if (paletteObject != null) {
                JSONObject primaryObject = paletteObject.optJSONObject("secondary");
                if (primaryObject != null) {
                  String  secondaryMainColor = primaryObject.optString("main");
                  themeConfig.setSecondaryMainColor(secondaryMainColor);
                    String secondaryContrastColor = primaryObject.optString("contrastText");
                    themeConfig.setSecondaryContrastColor(secondaryContrastColor);
                }
                JSONObject primaryPaletteObject = paletteObject.optJSONObject("primary");
                if (primaryPaletteObject != null) {
                    String primaryMainColor = primaryPaletteObject.optString("main");
                    themeConfig.setPrimaryMainColor(primaryMainColor);
                   String primaryContrastColor = primaryPaletteObject.optString("contrastText");
                    themeConfig.setPrimaryContrastColor(primaryContrastColor);
                }

            }
        }
    }

    public ThemeConfig getCaptureTheme(){
        return themeConfig;
    }

    public String getCaptureId(){
        return captureId;
    }

    public String getSessionId(){
        return sessionId;
    }

    private void setToken(String token){
        this.token = token;
    }

    public String getToken(){
        return token;
    }

    public String getStatus(){
        return status;
    }

    public void setLatitude(String latitude){
        this.latitude = latitude;
    }
    public void setLongitude(String longitude){
        this.longitude = longitude;
    }

    public void setAccuracy(String accuracy){
        this.accuracy = accuracy;
    }
    public void setAltitude(String altitude){
        this.altitude = altitude;
    }

    public void setSpeed(String speed){
        this.speed = speed;
    }

    public String getLatitude(){
        return latitude;
    }
    public String getLongitude(){
        return longitude;
    }

    public String getAccuracy(){
        return accuracy;
    }
    public String getAltitude(){
        return altitude;
    }
    public String getSpeed(){
        return speed;
    }
    public PageSequence getFirstPage(){
        return listPageSeq.get(0);
    }

    public PageSequence onNextPage(){
        pageIndex++;
        if (pageIndex < listPageSeq.size()) {
            return listPageSeq.get(pageIndex);
        }
        return new PageSequence("PAGE_END",null);
    }

    public void setRequestId(String requestId){
        this.requestId = requestId;
    }

    public String getRequestId(){
        return requestId;
    }



    public void getSessionToken(Context context, String requestId, String token, ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class).getSessionToken(requestId, token)
                .enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        try {
                            if (response.isSuccessful()) {
                                JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                                apiCallBack.onSessionSuccess(jsonObject);
                                //  successCallBack.onTokenReceived(sessionToken);

                            } else {
                                try {
                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                    Log.d("==SESSION",jObjError.toString());
                                    apiCallBack.onSessionFailure(jObjError.getString("reason"));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            apiCallBack.onSessionFailure(response.errorBody().toString());
                        }

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Log.d("==ERROR", t.getMessage());
                        apiCallBack.onSessionFailure(t.getMessage());

                    }
                });

    }

    public void initHealthCheck(Context context, String token, JsonObject object,
                                ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class)
                .initHealthCheck(token, object).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    //JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("===RES", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        apiCallBack.onSessionSuccess(jsonObject);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Log.d("==ERROR", String.valueOf(response.errorBody().toString()));
                    apiCallBack.onSessionFailure(response.errorBody().toString());

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiCallBack.onSessionFailure(t.getMessage());

            }
        });

    }

    public void submitHealthCheck(Context context, String token, JsonObject object,
                                  ApiCallBack apiCallBack) {
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class)
                .submitHealthCheck(token, object).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    //JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("===RES", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        apiCallBack.onSessionSuccess(jsonObject);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Log.d("==ERROR", String.valueOf(response.errorBody().toString()));
                    apiCallBack.onSessionFailure(response.errorBody().toString());

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiCallBack.onSessionFailure(t.getMessage());

            }
        });

    }

    public void uploadImage(Context context, String url, String path, ApiCallBack apiCallBack){
        File file = new File(path);
        InputStream in = null;
        RequestBody requestFile = null;
        try {
            in = new FileInputStream(file);
            byte[] buf;
            buf = new byte[in.available()];
            while (in.read(buf) != -1) ;
            requestFile = RequestBody.create(null, buf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RetrofitInstance.getRetrofitInstance(context).create(ApiInterface.class).uploadScreenShot(url,requestFile)
                .enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()){
                            apiCallBack.onSessionSuccess(null);
                        }else {
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                apiCallBack.onSessionFailure(jObjError.getString("message"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        apiCallBack.onSessionFailure(t.getMessage());

                    }
                });
    }

}
